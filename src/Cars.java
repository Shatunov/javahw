public class Cars {
    public String color = "Blue";
    public int Year = 2015;
    public String owner = "Salon";


    public Cars (String carColor, int carYear, String carOwner)
    {
        color = carColor;
        Year = carYear;
        owner = carOwner;
    }

    public Cars (String carOwner)
    {
        owner = carOwner;
    }
}
