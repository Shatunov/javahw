public class TasksAboutOOP {
    public static void main(String[] args) {
       /*1Task*/ Number number = new Number();
        System.out.println(number.x);

        /*2Task*/Print.PrintString(" for sure!");

        /*3Task*/Cars andreyCar = new Cars("Blue", 2008, "Me");
        System.out.println("Color is "+ andreyCar.owner + " and Year is " + andreyCar.Year + " and owner is " + andreyCar.owner);
    }
}