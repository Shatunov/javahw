import java.util.Scanner;

public class EvenString {
    public static void main(String[] args) {
        System.out.println("Enter a string"); //Suggest user to enter a string
        Scanner scanner = new Scanner(System.in);//Create a scanner
        String string = scanner.nextLine();
        int l = string.length();//Getting the length of the string
        if (l%2 ==0)//Getting the reminder and if the reminder is 0 printing out "String is even" if not printing out "string is not even"
        {
            System.out.println("String contains even number of characters");
        }
        else
        {
            System.out.println("String contains not even number of characters");
        }
    }
}
