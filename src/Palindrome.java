import java.util.Scanner;

class palindrome {
    public static void main(String[] args) {
        System.out.println("Enter a word to check");//Asking user to enter any word
        Scanner scanner = new Scanner(System.in);//Creating a scanner
        String word = scanner.nextLine();
        boolean flag = true;//Assigning "True" to boolean variable "flag" by default (suppose that word is Palindrome by default)
        int l = word.length();//Getting the length of the word
        for (int i = 0; i < l/2; i++)//Creating a Loop to check all symbols in th word and compare them
        {
            char a = Character.toLowerCase(word.charAt(i));//Gettin the 1st,the 2nd etc character till the half of the word
            char b = Character.toLowerCase(word.charAt(l-i-1));//Getting the last, the 1st before last etc characters .
            if (a != b)//if characters are not  equals - assign  false to the "flag" and stop the loop
            {
                flag = false;
                break;
            }
        }
        if (flag){//If flag is true - printing out "Word is a Palindrome"
            System.out.println("Word is a Palindrome");
        }
        else {//If flag is false - printing out "Word is not a Palindrome"
            System.out.println("Word is not a Palindrome");
        }
    }
}