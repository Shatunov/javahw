public class SetterAndGetter {



    public static void main(String[] args) {
        Person andrei = new Person();
        Person person1 = new Person();
        person1.setName("");
        person1.setLastname("Shatunov");
        person1.setAge(25);
        person1.speak();
        System.out.println("age "+person1.getAge());
    }

}


class Person {
    private String firstname = "default";
    private String lastname = "default";
    private int age = 0;


    public void setName(String username) {
        if (username.isEmpty())
        {
            System.out.println("It can't be empty");
        }

    else

    {
        firstname = username;
    }

}

    public String getName()
    {

        return firstname;
    }
    public void setLastname(String userlastname)
    {
        lastname = userlastname;
    }
    public  String getLastname()
    {
        return lastname;
    }
    public void setAge(int userage)
    {
        age = userage;
    }
    public int getAge()
    {
        return age;
    }

    void speak()
    {
        System.out.println("My name is " + firstname + " , my Lastname is " +lastname + ", my age is "+ age);
    }
}




