public class FizzBuzzRecursion {
    public static void main(String[] args) {
        Buzz(1);// Calling the method 'Buzz' starting with 'i' =1;
    }

    public static void Buzz(int i)
    {
        if (i>100)// if i is more the 100 - stop the recursion
        {
            return;
        }
        if (i%15 !=0 && i%5 !=0 && i%3 !=0 )//In all  cases except of deviding in 3/5/15 without reminder printing i;
        {System.out.println(i);}
        else if (i%15 == 0)//If dividing in 3 and 5 returns reminder equal 0 - printing "FizzBuzz"
        {System.out.println("FizzBuzz");}
         else if (i%3 ==0)//If dividing in 3  returns reminder equal 0 printing "Fizz"
        {System.out.println("Fizz");}
        else
        {System.out.println("Buzz");/*If dividing in 5 returns reminder equal 0 printing "Buzz" */}
        Buzz(i+1);// The function 'Buzz' calls itself during the 'i' is less than 101, and in each and every call encreases the 'i' in 1;
    }
}
